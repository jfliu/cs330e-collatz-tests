#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_for, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "1 999999\n"
        i, j = collatz_read(s)
        self.assertEqual(i,      1)
        self.assertEqual(j, 999999)

    def test_read_3(self):
        s = "1500 500\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1500)
        self.assertEqual(j,  500)

    def test_read_4(self):
        s = "4567 4567\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 4567)
        self.assertEqual(j, 4567)

    # ----
    # for
    # ----

    def test_for_1(self):
        v = collatz_for(400, 500)
        self.assertEqual(v, 142)

    def test_for_2(self):
        v = collatz_for(2048, 1024)
        self.assertEqual(v, 182)

    def test_for_3(self):
        v = collatz_for(999967, 999999)
        self.assertEqual(v, 259)
    
    def test_for_4(self):
        v = collatz_for(100, 100)
        self.assertEqual(v, 26)
    
    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_4(self):
        v = collatz_eval(1000, 900)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(1, 999999)
        self.assertEqual(v, 525)

    def test_eval_6(self):
        v = collatz_eval(4, 4)
        self.assertEqual(v, 3)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 169, 625, 144)
        self.assertEqual(w.getvalue(), "169 625 144\n")
    
    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 1000, 40, 179)
        self.assertEqual(w.getvalue(), "1000 40 179\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 1, 999999, 525)
        self.assertEqual(w.getvalue(), "1 999999 525\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("100 10\n300 40\n1370 400\n1025 925\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "100 10 119\n300 40 128\n1370 400 182\n1025 925 174\n")


    def test_solve_3(self):
        r = StringIO("6406 405\n3238 1528\n1030 4629\n3430 1080\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "6406 405 262\n3238 1528 217\n1030 4629 238\n3430 1080 217\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
